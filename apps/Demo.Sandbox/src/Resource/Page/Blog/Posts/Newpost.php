<?php

namespace Demo\Sandbox\Resource\Page\Blog\Posts;

use BEAR\Resource\ResourceObject;
use BEAR\Sunday\Inject\ResourceInject;
use BEAR\Sunday\Annotation\Form;

class Newpost extends ResourceObject
{
    use ResourceInject;

    public $body = [
        'errors' => ['title' => '', 'body' => ''],
        'submit' => ['title' => '', 'body' => '']
    ];

    public function onGet()
    {
        return $this;
    }

    /**
     * @param string $title
     * @param string $body
     * 
     * @Form
     */
    public function onPost($title, $body)
    {
        // create post
        $this->resource
            ->post
            ->uri('app://self/blog/posts')
            ->withQuery(['title' => $title, 'body' => $body])
            ->eager->request();

        // redirect
        $this->code = 303;
        $this->headers = ['Location' => '/blog/posts'];
        return $this;
    }
}
