<!DOCTYPE html>
<html lang="en">
<head>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container">
         <ul class="breadcrumb">
            <li><a href="/">Home</a> <span class="divider">/</span></li>
            <li><a href="/blog/posts">Blog</a> <span class="divider">/</span></li>
            <li class="active">Edit Post</li>
        </ul>
        
        <h1>Edit Post</h1>
        <form action="/blog/posts/edit" method="POST" role="form">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="id" value="{$id|escape}">

            <div class="form-group {if $errors.title}has-error{/if}">
                <label for="title">Title</label>
                <input type="text" id="title" name="title" value="{$submit.title|escape}" class="form-control">
                <label class="control-label" for="title">{$errors.title|escape}</label>
            </div>
            <div class="form-group {if $errors.body}has-error{/if}">
                <label for="body">Body</label>
                <textarea name="body" rows="10" cols="40" class="form-control" id="body">{$submit.body|escape}</textarea>
                <label class="control-label" for="body">{$errors.body|escape}</label>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</body>
</html>
