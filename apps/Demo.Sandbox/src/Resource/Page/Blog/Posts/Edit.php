<?php

namespace Demo\Sandbox\Resource\Page\Blog\Posts;

use BEAR\Resource\ResourceObject;
use BEAR\Sunday\Inject\ResourceInject;
use BEAR\Sunday\Annotation\Form;
use BEAR\Resource\Annotation\Embed;

class Edit extends ResourceObject
{
    use ResourceInject;

    public $body = [
        'errors' => ['title' => '', 'body' => '']
    ];

    /**
     * @Embed(rel="submit", src="app://self/blog/posts{?id}")
     */
    public function onGet($id)
    {
        $this['id'] = $id;

        return $this;
    }

    /**
     * @param int    $id
     * @param string $title
     * @param string $body
     *
     * @Form
     */
    public function onPut($id, $title, $body)
    {
        // update post
        $this->resource
            ->put
            ->uri('app://self/blog/posts')
            ->withQuery(['id' => $id, 'title' => $title, 'body' => $body])
            ->eager
            ->request();

        // redirect
        $this->code = 303;
        $this->headers = ['Location' => '/blog/posts'];

        return $this;
    }
}
