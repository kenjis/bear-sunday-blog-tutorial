<!DOCTYPE html>
<html lang="en">
<head>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">Home</a> <span class="divider">/</span></li>
            <li class="active">Blog</li>
        </ul>
        
        <h1>Posts</h1>
        {$posts}
        <a href="posts/newpost" class="btn btn-primary btn-large">New Post</a>
    </div>
</body>
</html>
