<?php

namespace Demo\Sandbox\Resource\Page\Blog;

use BEAR\Resource\ResourceObject;
use BEAR\Sunday\Inject\ResourceInject;
use BEAR\Sunday\Annotation\Cache;

class Posts extends ResourceObject
{
    use ResourceInject;

    /**
     * @Cache
     */
    public function onGet()
    {
        $this['posts'] = $this->resource->get->uri('app://self/blog/posts')->request();
        return $this;
    }
}
