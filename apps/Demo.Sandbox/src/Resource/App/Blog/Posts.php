<?php

namespace Demo\Sandbox\Resource\App\Blog;

use BEAR\Sunday\Annotation\Db;
use BEAR\Resource\ResourceObject;
use BEAR\Package\Module\Database\Dbal\Setter\DbSetterTrait;
use PDO;
use BEAR\Sunday\Annotation\Cache;
use BEAR\Sunday\Annotation\CacheUpdate;

/**
 * @Db
 */
class Posts extends ResourceObject
{
    use DbSetterTrait;

    /**
     * Current time
     *
     * @var string
     */
    public $time;

    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @param int $id
     * 
     * @Cache(100)
     */
    public function onGet($id = null)
    {
        $sql = "SELECT id, title, body, created, modified FROM {$this->table}";
        if (is_null($id)) {
            $stmt = $this->db->query($sql);
            $this->body = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $this;
        }

        $sql .= " WHERE id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('id', $id);
        $stmt->execute();
        $this->body = $stmt->fetch(PDO::FETCH_ASSOC);

        return $this;
    }

    /**
     * @CacheUpdate
     */
    public function onPost($title, $body)
    {
        $values = [
            'title' => $title,
            'body' => $body,
            'created' => $this->time,
        ];
        $this->db->insert($this->table, $values);
        $this->code = 204;

        return $this;
    }

    /**
     * @CacheUpdate
     */
    public function onDelete($id)
    {
        $this->db->delete($this->table, ['id' => $id]);
        $this->code = 204;

        return $this;
    }

    /**
     * @param int    $id
     * @param string $title
     * @param string $body
     * 
     * @CacheUpdate
     */
    public function onPut($id, $title, $body)
    {
        $values = [
            'title' => $title,
            'body' => $body,
            'modified' => $this->time
        ];
        $this->db->update($this->table, $values, ['id' => $id]);
        $this->code = 204;

        return $this;
    }
}
