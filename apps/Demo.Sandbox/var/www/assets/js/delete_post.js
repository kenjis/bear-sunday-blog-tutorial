$(document).ready(function () {
    $('.remove-confirm').click(function (event) {
        event.preventDefault();
        c = confirm('Are you sure you want to delete this?');
        if (c) {
            var postId = $(event.target).data('postId');
            $.ajax({
                url: '/blog/posts/post',
                type: "POST",
                headers: {
                    'X-HTTP-Method-Override': 'DELETE'
                },
                data: 'id=' + postId,
                success: function () {
                    window.location = "/blog/posts";
                }
            });
        }
    });
});
