# BEAR.Sunday Blog Tutorial (Under Construction)

## Installation

~~~
$ git clone git@bitbucket.org:kenjis/bear-sunday-blog-tutorial.git
$ cd bear-sunday-blog-tutorial
$ composer install
~~~

## How to clear cache

~~~
$ php apps/Demo.Sandbox/bin/clear.php
~~~

## Run web server

~~~
$ bin/bear.server apps/Demo.Sandbox/
~~~

Browse http://localhost:8080/blog/posts
